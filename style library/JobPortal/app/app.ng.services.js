﻿(function() {
    "use strict";

    jpApp.factory("svc", ["$http", "$q", function ($http, $q) {
        var _siteUrl = _spPageContextInfo.siteServerRelativeUrl
        var _getConfig = {
            headers: { "accept": "	application/json;odata=verbose" }
        }
        var imgRegex = /<img.*?src="(.*?)"/;

        return {
            getTemplateUrl: getTemplateUrl,
            getJobCategories: getJobCategories,
            getBlogPosts: getBlogPosts
        }

        function getTemplateUrl(templateName) {
            return _siteUrl + "style library/jobportal/app/templates/" + templateName;
        }

        function getJobCategories(count) {
            var url = _siteUrl+ "_api/web/lists/getbytitle('JobCategories')/items?$orderby=Order0&$top="+ count;
            return _getSPItems(url).then(function (d) { return d });
        }

        function getBlogPosts(count) {
            var url = _siteUrl+ "_api/web/lists/getbytitle('BlogPosts')/items?$orderby=jpPublishDate desc&$top=" + count;
            return _getSPItems(url).then(function (d) {
                var col = d;
                col.forEach(function (item) {
                    _getJpImageUrl(item.FieldValuesAsHtml.__deferred.uri).then(function (u) {
                        item.imageUrl = u;
                    });
                });

                return col;
            });
        }

        function _getSPItems(endpoint) {
            return $http.get(endpoint, _getConfig).then(function (d) { return d.data.d.results });
        }

        function _getJpImageUrl(endpoint) {
            return $http.get(endpoint, _getConfig).then(function(d) {
                var imgTag = d.data.d.jpImage;
                var src = '';
                if (imgTag) {
                    src = (imgRegex.exec(imgTag) !== null) ? imgRegex.exec(imgTag)[1] : '';
                    src = (src.indexOf("?") > 0) ? src.substring(0, src.indexOf("?")) : src;
                }
                return src;
            });
        }
    }]);

})();
﻿(function () {
    "use strict";
    jpApp.config(["$routeProvider", function ($routeProvider) {
        $routeProvider.when("/detail/:id", {
            templateUrl: "../Templates/people/all.html",
            controller: "allPeopleCtrl"
        });
    }]);
})();
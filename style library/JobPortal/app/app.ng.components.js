﻿(function() {
    "use strict";

    jpApp.component("jpTopNav", {
        controllerAs: "vm",
        controller: function (svc) {
            var vm = this;
            vm.templateUrl = svc.getTemplateUrl("topNav.html");
        },
        template: "<div ng-include='vm.templateUrl'></div>"
    })

    jpApp.component("jpHeader", {
        controllerAs: "vm",
        controller: function (svc) {
            var vm = this;
            vm.templateUrl = svc.getTemplateUrl("glb-header.html");
        },
        template: "<div ng-include='vm.templateUrl'></div>"
    })

    jpApp.component("jpFooter", {
        controllerAs: "vm",
        controller: function (svc) {
            var vm = this;
            vm.templateUrl = svc.getTemplateUrl("glb-footer.html");
        },
        template: "<div ng-include='vm.templateUrl'></div>"
    })

    jpApp.component("jpJobCategories", {
        controllerAs: "vm",
        controller:  function (svc) {
            var vm = this;
            vm.templateUrl = svc.getTemplateUrl("jobs-categories.html");
            vm.$onInit = function () {
                svc.getJobCategories(vm.count || 8).then(function (f) { vm.items = f });
            }
        },
        template: "<div ng-include='vm.templateUrl'></div>",
        bindings: {
            count: "@"
        }
    })

    jpApp.component("jpBlogLatest", {
        controllerAs: "vm",
        controller: function (svc) {
            var vm = this;
            vm.templateUrl = svc.getTemplateUrl("blog-latest.html");
            vm.$onInit = function () {
                svc.getBlogPosts(vm.count || 4).then(function(f) { vm.items = f });
            }
        },
        template: "<div ng-include='vm.templateUrl'></div>",
        bindings: {
            count: "@"
        }
    })
})();